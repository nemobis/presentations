# Presentations by Federico Leva

Various presentations mostly in markdown format. Because I'm unhappy about most presentation slides templates.

## Make remark

On Fedora,

```
sudo dnf install perl-Template-Toolkit
wget https://remarkjs.com/downloads/remark-latest.min.js -O remark.js
```

## Future work

* Switch to [remark](https://github.com/gnab/remark/wiki#getting-started) with [make](https://github.com/gnab/remark/issues/479#issuecomment-341213187) or possibly Hugo.
* Recover some old presentation slides?
* Figure out how to export to PDF/A.

## License

Copyright Federico Leva 2023, [CC BY-SA 4.0](https://creativecommons.org/licenses/by-sa/4.0/).
