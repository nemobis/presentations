[% INCLUDE "template.html" %]
[% BLOCK content %]

class: center, middle

# Some easy contributions to FLOSS from a thousand-employee corporation

Federico Leva, [CC BY-SA 4.0](https://creativecommons.org/licenses/by-sa/4.0/)

FOSDEM 2024

---
## Introductions

* Federico Leva ([federicoleva.eu](https://federicoleva.eu/))
* DevOps Engineer at [RELEX Solutions](https://www.relexsolutions.com/)
* At FOSDEM with [Wikimedia](https://www.wikimedia.org/) friends: [w.wiki/8PEc](https://www.mediawiki.org/wiki/Events/FOSDEM/2024)

---
## RELEX Solutions

* Finnish SaaSS company founded in 2005; almost 2000 employees globally, ~200 M€/y revenue, profitable.
* «The market-leading supply chain and retail planning platform: We help retailers and consumer brands unify their planning, from demand and merchandise to supply chain and operations, for maximum customer satisfaction at the lowest operating cost» ([relexsolutions.com](https://www.relexsolutions.com/)).
* Many FLOSS components: from Kubernetes and friends to Haskell, Nix, terraform, ansible etc.
  * https://www.relexsolutions.com/careers/blog/cloud-devops/
  * https://www.relexsolutions.com/careers/blog/tech-life-at-relex-deployment/
* Observability: Prometheus, Grafana, VictoriaMetrics, OpenTelemetry, Fluentd and more [CNCF](https://www.cncf.io/) projects.

---
## Observability

* «Observability lets us understand a system from the outside, by letting us ask questions about that system without knowing its inner workings. Furthermore, it allows us to easily troubleshoot and handle novel problems (i.e. "unknown unknowns"), and helps us answer the question, "Why is this happening?"».
  * https://opentelemetry.io/docs/concepts/observability-primer/#what-is-observability
  * Cf. [CNCF TAG Observability](https://github.com/cncf/tag-observability)
* A method to solve a [collective action problem](https://en.wikipedia.org/wiki/Collective_action_problem) by collecting and summarising information to reduce information costs (cf. Elinor Ostrom, Douglass North).
* Pretty charts! (Check [grafana.wikimedia.org](https://grafana.wikimedia.org/).)

---
## Pretty charts

.center[![:img Grafana chart showing some statistics about a VictoriaMetrics cluster, 75%](static/Screenshot_20231203_224636.png)]

---
## Give money

.center[![:img RELEX at OpenCollective, 75%](static/Screenshot_20231205_143949.png)]

https://opencollective.com/relex

---
## Publish your components as free software

.center[![:img relex/aini, 75%](static/Screenshot_20231203_212232.png)]

[relex/aini](https://github.com/relex/aini)

---
## Upstream your patches

.center[![:img Allow TLS to use keep-alive socket option, 75%](static/Screenshot_20231203_212635.png)]

[fluent/fluentd/pull/3308](https://github.com/fluent/fluentd/pull/3308)

---
## File bug reports

.center[![:img Error out of bounds may disable all jobs when invalid metrics are received by remote-write, 75%](static/Screenshot_20231203_212044.png)]

[prometheus/prometheus/issues/12006](https://github.com/prometheus/prometheus/issues/12006)

---
## File accessibility reports

.center[![:img A11y: Explore page text contrast too low for WCAG AA/AAA, 75%](static/Screenshot_20231203_212120.png)]

[grafana/grafana/issues/57984](https://github.com/grafana/grafana/issues/57984)

---
## File feature requests

.center[![:img Expose storage.tsdb.retention.time in a metric, 75%](static/Screenshot_20231203_212202.png)]

[prometheus/prometheus/issues/12925](https://github.com/prometheus/prometheus/issues/12925)

---
## Document your process and learnings

.center[![:img Open source contributions documentation page, 95%](static/Screenshot_20231203_223014.png)]

* «I have an opinion I want to force on others! How can I do that without the wiki?!» ([emacsen quip](https://bash.toolforge.org/quip/AU7VVk4U6snAnmqnK_yd))

---
## Help colleagues with bureaucracy

.center[![:img LFC insights organization contribution index, 90%](static/Screenshot_20231203_224214.png)]

https://lfx.linuxfoundation.org/tools/easycla

---
## Contribute to support forums

.center[![:img Terraform 1.1.7 Error Backend configuration changed, 75%](static/Screenshot_20231204_135331.png)]

https://stackoverflow.com/a/72689134/1333493

---
## Thanks

Thanks to my employer RELEX Solutions for covering my flight-less travel from Helsinki through Stockholm and Berlin (with SJ and [EuropeanSleeper](https://www.europeansleeper.eu/)).

[#StayGrounded](https://mamot.fr/tags/StayGrounded)

Not affiliated with or otherwise sponsored by CNCF.

[% END %]
