# FOSDEM 2024 slides

## Abstract

As a free software contributor, I've often discussed how corporations and the public sector should use FLOSS more and contribute to it. But how to turn the principle into reality when I'm on the other side and I work in a relatively big proprietary software shop, which just happens to use some free software?

I'll give a few examples of "obvious" things you can do, to show anyone can make things a bit better.

* Publish your components as free software
* Upstream your patches
* File bug reports
* File accessibility reports
* File feature requests
* Document your process and learnings
* Help colleagues with bureaucracy
* Contribute to support forums

The examples are from direct experience with free software for observability, namely [Grafana](https://github.com/grafana/grafana/), [Prometheus](https://github.com/prometheus/prometheus/), [VictoriaMetrics](https://github.com/VictoriaMetrics/VictoriaMetrics/), [fluentd](https://github.com/fluent/fluentd), as well as our own [aini](https://github.com/relex/aini). Grafana and Prometheus are an example of public good as millions rely on them to keep their inboxes clean.

Slides available at https://framagit.org/nemobis/presentations
