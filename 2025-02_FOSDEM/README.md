# FOSDEM 2025 slides

## Abstract

In complex software architectures, observability needs are ever growing and tend to produce massive amounts of logs and metrics. How to keep volumes in check while making observability easy to use? One possible approach is a central observability platform to handle the aggregation of all metrics and logs produced across a company or project. Knowledge sharing and optimisations become easier with shared infrastructure. Products like VictoriaMetrics and Vector.dev are easy to scale and offer features like [Streaming Aggregation](https://docs.victoriametrics.com/stream-aggregation/). This presentation will show a concrete example of their use to support a Grafana instance used by dozens of teams and hundreds of users at a global software company based in Finland.

The presentation will not explain the most common concepts and tools for telemetry and logging (like Grafana, Prometheus, OpenTelemetry), but some pointers will be provided.

Slides available at https://framagit.org/nemobis/presentations
