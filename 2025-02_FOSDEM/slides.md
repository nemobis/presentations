[% INCLUDE "template.html" %]
[% BLOCK content %]

class: center, middle

# Telemetry and log aggregation for more collaborative observability (WIP)

Federico Leva, [CC BY-SA 4.0](https://creativecommons.org/licenses/by-sa/4.0/)

FOSDEM 2025

---
## Introductions

* Federico Leva ([federicoleva.eu](https://federicoleva.eu/))
* Free Software enthusiast mostly around [Wikimedia](https://www.wikimedia.org/) (our meetup: [w.wiki/CG7k](https://www.mediawiki.org/wiki/Events/FOSDEM/2025))
* Senior DevOps Engineer at [RELEX Solutions](https://www.relexsolutions.com/)
* Only my personal opinions etc.

---
## RELEX Solutions

* Finnish SaaSS company founded in 2005; almost 2000 employees globally, ~200 M€/y revenue, profitable.
  * If you visit any major supermarket chain in Europe, you're probably an indirect user.
  * B2B software, each customer often has very few users interacting directly with it.
* Hybrid cloud with many FLOSS components (usual Linux, Kubernetes things but also Haskell, Nix and more).
* Observability: Prometheus, Grafana, VictoriaMetrics, OpenTelemetry, Fluent, Vector and more.
* Some FLOSS contributions: [github.com/relex](https://github.com/relex).
* Hiring: [www.relexsolutions.com/careers/](https://www.relexsolutions.com/careers/).

---
## Observability

* «Observability lets us understand a system from the outside, by letting us ask questions about that system without knowing its inner workings. Furthermore, it allows us to easily troubleshoot and handle novel problems (i.e. "unknown unknowns"), and helps us answer the question, "Why is this happening?"».
  * https://opentelemetry.io/docs/concepts/observability-primer/#what-is-observability
  * Cf. [CNCF TAG Observability](https://github.com/cncf/tag-observability)
* A method to solve a [collective action problem](https://en.wikipedia.org/wiki/Collective_action_problem) by collecting and summarising information to reduce information costs (cf. Elinor Ostrom, Douglass North).
* Pretty charts! (Check [grafana.wikimedia.org](https://grafana.wikimedia.org/).)

---
## Defining availability

* Not just whether a UI loads in the user's browser.
* Examples: success of a daily calculation (scheduled job)
  * Send some output to the customer's supplier by 4 am
  * Provide the user with some output by 8 am

---
## Scale of the issue

* Chief application is Java 21 on baremetal RHEL servers
  * Kotlin, Scala, Jruby, RoR, GraphQL, React, Typescript
  * Deployed with Gitlab, Ansible, Nix, Haskell
* Over 100k CPU cores, PBs of memory and data
* 2M samples/second, TBs in metrics, TBs/month in ingested logs
  * Respectable amounts, by no means unique

---
## Carbon and cost considerations

* Financial costs of rented computing (public cloud) are easy to look at
  * Example measure: are costs of the observability platform under 1 % of the total?
* Peak memory usage tends to dominate sizing and carbon impact
* CPU usage is difficult to to translate to emissions
  * https://github.com/cloud-carbon-footprint/cloud-carbon-footprint/issues/1060

Climate change is the challenge of our lifetime. Keep the [IPCC AR6](https://www.ipcc.ch/synthesis-report/) in mind!

---
## Grafana and much more

* Standard exporters
* Custom exporters
* Custom HTTP discovery
* ...

---
## Vector for log aggregation

---
## VictoriaMetrics as central metrics storage

* vmagent

---
## Downsampling and tail sampling

* Easier with a centralised model
  * Statical pitfalls
  * Load balancing 
* Cf. Benedikt Bongartz and Julius Hinze, "[Strategic Sampling](https://archive.fosdem.org/2024/schedule/event/fosdem-2024-3445-strategic-sampling-architectural-approaches-to-efficient-telemetry/)", FOSDEM 2024

---
## Autoscaling considered boring

* Observability platform load dominated by data ingestion
  * More data writing than reading
  * Telemetry volumes quite constant during the day
  * Log ingestion varies more
* Cf. [Use one big server](https://www.specbranch.com/posts/one-big-server/)
* Will hit physical limits at some point

---
## Redundancy and limitations

* Ingestion queues as implicit redundancy
* Disaster recovery and failover
* Backups

---
## Collaboration in shared observability configuration

* 
* InnerSource?
* Ask forgiveness rather than permission

---
## Thanks

Thanks to my employer RELEX Solutions for covering my flight-less travel from Helsinki through Stockholm and Berlin.

[#StayGrounded](https://mamot.fr/tags/StayGrounded)

Not affiliated with or otherwise sponsored by CNCF, VictoriaMetrics, Vector or others mentioned.

[% END %]
